@extends('layouts.tienda')

@section('content')

@if(Session::has('message'))
	<div class="alert alert-success alert-dismissible" role="alert">
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  {{Session::get('message')}}
	</div>
@endif

	<h3 class="text-primary text-center">Productos de la tienda</h3>
	<table class="table table-bordered">
		<thead>
			<th>Nombre del producto</th>
			<th>Cantidad</th>
			<th>Operaciones</th>
		</thead>
		@foreach($tienda as $dato)
			<tbody>
				<td>{{$dato->nom_produc}}</td>
				<td>{{$dato->cantidad}}</td>
				<td>{!!link_to_route('tienda.edit',$title='Editar',$parameters = $dato->id,$attributes = ['class'=>'btn btn-primary'])!!}</td>
			</tbody>
		@endforeach
	</table>
@stop