@extends('layouts.tienda')
@section('content')
@include('alerts.request')
	{!!Form::model($producto,['route'=>['tienda.update',$producto->id],'method'=>'PUT'])!!}
		@include('tienda.forms.store')
		{!!Form::submit('Actualizar',['class'=>'btn btn-seccess'])!!}
	{!!Form::close()!!}


	{!!Form::open(['route'=>['tienda.destroy',$producto->id],'method'=>'DELETE'])!!}
		{!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
	{!!Form::close()!!}
@stop