@extends('layouts.tienda')

@section('content')
	@include('alerts.request')
	{!!Form::open(['route'=>'tienda.store','method'=>'POST'])!!}
		@include('tienda.forms.store')
		{!!Form::submit('Registrar',['class'=>'btn btn-primary'])!!}
	{!!Form::close()!!}
@stop


 <!--label >Tipo</label>
		    <select class="form-control" name="tipo" id="">
		    	<option value="Seleccione">Seleccione</option>
		    	<option value="Alimentacion">Alimentacion</option>
		    	<option value="Limpieza">Limpieza</option>
		    	<option value="Vegetales">Vegetales</option>
		    </select-->