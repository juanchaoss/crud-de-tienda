<div class="form-group">
   {!!Form::label('Nombre')!!}
   {!!Form::text('nom_produc',null,['class'=>'form-control','placeholder'=>'Nombre del producto'])!!}
</div>
<div class="form-group">
   {!!Form::label('Cantidad')!!}
   {!!Form::text('cantidad',null,['class'=>'form-control','placeholder'=>'cantidad'])!!}
</div>
<div class="form-group">
   {!!Form::label('Tipo')!!}
   {!!Form::select('tipo', array('Limpieza' => 'Limpieza', 'Alimentacion' => 'Alimentacion', 'Ropa' => 'Ropa'))!!}
</div>
