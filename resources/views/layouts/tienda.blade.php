<html>
	<head>
		<title>Tienda</title>
		{!!Html::style('recursos/bootstrap.css')!!}
	</head>
	<body>
		<h3 class="text-center">CRUD Ejemplo de una Tienda</h3>
		<div class="col-md-12">
			&nbsp;
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					@yield('content')
					<div class="btn-group">
						<a href="{!!URL::to('/tienda/create')!!}" class="btn btn-primary">Guardar Producto</a>
						<a href="{!!URL::to('/tienda/')!!}" class="btn btn-success">Listar Productos</a>
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>
			<button id="br" class="btn btn-default"> prueba</button>
		</div>
		{!!Html::script('recursos/jquery.js')!!}
		{!!Html::script('recursos/bootstrap.js')!!}
		<script>
			$(document).on('ready',function(){
				$("#br").on('click',function(){
					alert("Hiciste click en el boton...Yeah! /,,/")
				});
			});
		</script>
	</body>
</html>