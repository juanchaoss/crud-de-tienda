<?php

namespace tienda;

use Illuminate\Database\Eloquent\Model;

class Tienda extends Model
{
    protected $table = 'store';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nom_produc', 'cantidad', 'tipo'];

    protected $hidden = ['nom_produc', 'cantidad', 'tipo'];
}
