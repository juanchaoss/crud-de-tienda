<?php

namespace tienda\Http\Requests;

use tienda\Http\Requests\Request;

class TiendaUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom_produc' => 'required',
            'cantidad'   => 'required',
            'tipo'       => 'required',
        ];
    }
}
