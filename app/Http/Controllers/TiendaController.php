<?php

namespace tienda\Http\Controllers;

use Illuminate\Http\Request;
use tienda\Tienda;
use tienda\Http\Requests;
use tienda\Http\Requests\TiendaUpdateRequest;
use tienda\Http\Requests\TiendaCreateRequest;
use tienda\Http\Controllers\Controller;
/*use Illuminate\Support\Farcades\Session;
use Illuminate\Support\Farcades\Redirect;*/
use Session;
use Redirect;

class TiendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tienda = Tienda::All();
        return view('tienda.index',compact('tienda'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('tienda.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(TiendaCreateRequest $request)
    {
        Tienda::create([  //<---- usando namespace
            'nom_produc' => $request['nom_produc'],
            'cantidad'   => $request['cantidad'],
            'tipo'       => $request['tipo'],
        ]);

        return redirect('/tienda')->with('message','store');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $producto = Tienda::find($id);
        return view('tienda.edit',['producto'=>$producto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(TiendaUpdateRequest $request, $id)
    {
        $producto = Tienda::find($id);
        $producto->fill($request->all());
        $producto->save();

        Session::flash('message','Producto editado correctamente');
        return Redirect::to('/tienda');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Tienda::destroy($id);

        Session::flash('message','Producto Eliminado correctamente');
        return Redirect::to('/tienda');
    }
}
